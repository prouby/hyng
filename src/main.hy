#! /usr/bin/env hy

(import sys os pygame getopt time)

(import [pong [*]])
(import [sock [*]])
(import [protocol [*]])
(import [truelisp [*]])
(require [truelisp [*]])

(setv version "0.1")

(defn usage [args]
  (print (+ "Usage: " (car args) " [options] --port PORT"))
  (print "  -s, --server     Run as a server")
  (print "  -c, --client     Run as a client")
  (print "  -h, --host       Host ip addr (optional if localhost)")
  (print "  -p, --port       Port number")
  (print "  --ssl            Use ssl connection (not inplemented yet)")
  (print "  --help           Show this help")
  ((. sys exit) 1))

(defn get_options [args]
  (let [[short_options "svh:p:"]
        [long_options  ["server" "client" "host=" "port=" "ssl" "help"]]
        [host   "127.0.0.1"]
        [port   6666]
        [server True]
        [ssl    False]]
       (do
         (try
           (setv opts (car (getopt.getopt (cdr args)
                                          short_options
                                          long_options)))
           (except [err [getopt GetoptError]]
             (usage args)))

         (for [x opts]
           (setv o (car x))
           (setv a (car (cdr x)))

           (cond [(or (= o "-s") (= o "--server"))
                  (setv server True)]
                 [(or (= o "-c") (= o "--client"))
                  (setv server False)]
                 [(or (= o "-h") (= o "--host"))
                  (setv host a)]
                 [(or (= o "-p") (= o "--port"))
                  (setv port a)]
                 [(= o "--ssl")
                  (setv ssl True)]
                 [(= o "--help")
                  (usage args)]
                 [True
                  (usage args)]))

         [host port server ssl])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main                                                                         ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmain [&rest args]
  (let [[conf       (get_options args)]
        [host       (car conf)]
        [port       (int (nth conf 1))]
        [server     (nth conf 2)]
        [ssl        (nth conf 3)]
        [connection (Sock host port :server server :tcp True :ssl ssl)]
        [session    (Game server)]
        [multi      (Protocol connection session)]]
       (do
         ((. multi connection) version)
         ((. session ball throw))

         (while True
           (let [[start  ((. time time))]
                 [events ((. pygame event get))]
                 [retl   (list
                           (remove (λ [x] (= x "nope"))
                                   (map-list (λ [e] ((. session event) e))
                                             events)))]
                 [ret    (if (= retl []) "" (car retl))]]
                (do
                  (if (not (or (= ret "nope")
                               (= ret "")))
                      ((. multi send_move_command) ret))

                  ((. multi recv_command))

                  (setv act ((. session draw)))

                  (cond [(= act "1 lost")
                         ((. multi send_throw_command))]
                        [(= act "2 lost")
                         ((. multi send_throw_command))]
                        [(= act "touch")
                         ((. multi send_touch_command))])

                  (setv stop ((. time time)))
                  ((. session delay) (- stop start))))))))
